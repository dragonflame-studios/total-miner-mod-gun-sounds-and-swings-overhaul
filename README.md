**To download this mod, please visit the [Mod Releases Page](https://gitlab.com/dragonflame-studios/total-miner-mod-gun-sounds-and-swings-overhaul/-/releases)**

Gun Sounds and Swings Overhaul is a simple XML mod for the former Xbox 360, and now PC game Total Miner.

This mod of mine changes the various gun swing timings within Total Miner to what I _subjectively_ feel is appropriate. It also changes all of the guns' sounds to the same single gunshot sound, which is better for repeated rapid fire.

All of the guns except the two laser guns are touched by the overhaul. 

**Be VERY careful with the Grenade Launcher. For shits and giggles, I made it super fast like the Minigun too.**
